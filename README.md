**Build Status**

* master

[![pipeline status](https://gitlab.com/master-cloud-apps/02_servicios_internet/02_01_tecnologias_servicios_internet/practica-01-book-manager/badges/master/pipeline.svg)](https://gitlab.com/master-cloud-apps/02_servicios_internet/02_01_tecnologias_servicios_internet/practica-01-book-manager/-/commits/master)
[![coverage report](https://gitlab.com/master-cloud-apps/02_servicios_internet/02_01_tecnologias_servicios_internet/practica-01-book-manager/badges/master/coverage.svg)](https://gitlab.com/master-cloud-apps/02_servicios_internet/02_01_tecnologias_servicios_internet/practica-01-book-manager/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=org.eyo%3Abook-manager&metric=alert_status)](https://sonarcloud.io/dashboard?id=org.eyo%3Abook-manager)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=org.eyo%3Abook-manager&metric=coverage)](https://sonarcloud.io/dashboard?id=org.eyo%3Abook-manager)



# Book Manager

Proyecto con el manejador de libros online de la primera práctica
de la asignatura de Tecnologías de Servicios de Internet
del Máster de Cloud Apps de la URJC.

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#using-boot-devtools)
* [Mustache](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-spring-mvc-template-engines)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

