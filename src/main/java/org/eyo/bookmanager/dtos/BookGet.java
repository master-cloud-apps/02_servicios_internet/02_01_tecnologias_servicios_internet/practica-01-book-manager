package org.eyo.bookmanager.dtos;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;

import java.util.List;

public class BookGet {
    private Book book;

    public BookGet(Book book) {
        this.book = book;
    }

    public Long getId(){
        return this.book.getId();
    }

    public String getTitle(){
        return this.book.getTitle();
    }

    public List<Comment> getComments(){
        return this.book.comments();
    }

    public String getReview() {
        return this.book.review();
    }

    public String getAuthor() {
        return this.book.author();
    }

    public String getEditorial() {
        return this.book.editorial();
    }

    public Long getYearPublication() {
        return this.book.yearPublication();
    }

}
