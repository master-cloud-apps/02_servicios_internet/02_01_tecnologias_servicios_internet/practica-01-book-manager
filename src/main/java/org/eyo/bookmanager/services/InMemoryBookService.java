package org.eyo.bookmanager.services;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class InMemoryBookService implements BookService {
    private AtomicLong nextId = new AtomicLong(1);
    private ConcurrentMap<Long, Book> books = new ConcurrentHashMap<>();

    @Override
    public void save(Book book) {
        if (book.getId() == null || book.getId() == 0) {
            book.setId(nextId.getAndIncrement());
        }
        this.books.put(book.getId(), book);
    }

    @Override
    public Collection<Book> getAll() {
        return this.books.values();
    }

    @Override
    public Book findById(Long bookId) {
        return this.books.get(bookId);
    }

    @Override
    public void deleteById(Long bookId) {
        this.books.remove(bookId);
    }

    @Override
    public void addCommentToBook(Long bookId, Comment comment) {
        Book bookToComment = this.books.get(bookId);
        bookToComment.setComment(comment);
    }
}
