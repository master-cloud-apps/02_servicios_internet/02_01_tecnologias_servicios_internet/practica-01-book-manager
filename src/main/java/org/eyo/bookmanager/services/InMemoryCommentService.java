package org.eyo.bookmanager.services;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class InMemoryCommentService implements CommentService {
    private AtomicLong nextId = new AtomicLong(1);
    private ConcurrentMap<Long, Comment> comments = new ConcurrentHashMap<>();
    private BookService bookService;

    public InMemoryCommentService(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public Collection<Comment> getAll(Long bookId) {
        Optional<Book> bookWithComments = this.bookService.getAll().stream()
                .filter(book -> book.getId().equals(bookId))
                .findAny();
        if (bookWithComments.isPresent()){
            return bookWithComments.get().comments();
        }
        return Collections.emptyList();
    }

    @Override
    public void save(Long bookId, Comment comment) {
        if (comment.getId() == null || comment.getId() == 0) {
            comment.setId(nextId.getAndIncrement());
        }
        this.comments.put(comment.getId(), comment);
        this.bookService.addCommentToBook(bookId, comment);
    }

    @Override
    public void deleteComment(Long bookId, Long commentId) {
        this.comments.remove(commentId);
        Book bookFromComment = this.bookService.findById(bookId);
        bookFromComment.deleteComment(commentId);
        this.bookService.save(bookFromComment);
    }

    @Override
    public Comment finById(Long commentId) {
        return this.comments.get(commentId);
    }
}
