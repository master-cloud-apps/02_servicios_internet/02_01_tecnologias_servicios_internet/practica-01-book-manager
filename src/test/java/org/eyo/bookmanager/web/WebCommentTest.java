package org.eyo.bookmanager.web;

import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class WebCommentTest extends WebTest {

    @Test
    void given_create_book_should_return_ok_when_create_comment() throws Exception {
        String commentContent = new CommentBuilder()
                .setContent("test_content")
                .setName("Juan Carballeira")
                .setPunctuation(4.5F).build();
        Long bookId = createBook(new BookBuilder().build());

        Long commentId = createComment(commentContent, bookId);

        this.mockMvc.perform(get("/books/" + bookId + "/comments/" + commentId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").value("test_content"))
                .andExpect(jsonPath("$.name").value("Juan Carballeira"))
                .andExpect(jsonPath("$.punctuation").value(4.5F));


        this.mockMvc.perform(delete("/books/" + bookId));
    }

    @Test
    void given_no_comment_created_when_get_comment_should_return_not_found() throws Exception {
        this.mockMvc.perform(get("/books/1/comments/12345678"))
                .andExpect(status().isNotFound());
    }
}
