package org.eyo.bookmanager.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest
public abstract class ControllerTest {
    @Autowired
    protected BookController bookController;

    protected Long getIdFromLocation(String locationHeader) {
        return Long.parseLong(locationHeader.substring(locationHeader.lastIndexOf('/') + 1));
    }

    protected Long getIdFromResponse(ResponseEntity response){
        return this.getIdFromLocation(response.getHeaders().get("location").get(0));
    }

    protected void deleteBooks(List<Long> listIds){
        listIds.stream().forEach(this.bookController::deleteBookById);
    }
}
