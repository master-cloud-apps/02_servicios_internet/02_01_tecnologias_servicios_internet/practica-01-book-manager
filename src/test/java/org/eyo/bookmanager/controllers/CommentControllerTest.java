package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CommentControllerTest extends ControllerTest {

    @Autowired
    private CommentController commentController;

    @Test
    void should_return_location_when_create_comment_over_book() {
        ResponseEntity bookResponse = this.bookController.createBook(new Book("Test title"));
        Long bookId = getIdFromLocation(bookResponse.getHeaders().get("location").get(0));

        ResponseEntity commentResponse = this.commentController.createComment(bookId, new Comment());
        this.deleteBooks(Arrays.asList(bookId));

        assertEquals(HttpStatus.CREATED, commentResponse.getStatusCode());
        assertNotNull(commentResponse.getHeaders().get("location"));
    }

    @Test
    void should_return_no_content_when_delete_comment_created() {
        ResponseEntity bookResponse = this.bookController.createBook(new Book("Test title"));
        Long bookId = getIdFromResponse(bookResponse);
        ResponseEntity commentResponse = this.commentController.createComment(bookId, new Comment());
        Long commentId = getIdFromResponse(commentResponse);

        ResponseEntity commentDeletedResponse = this.commentController.deleteCommentOverBook(bookId, commentId);

        this.deleteBooks(Arrays.asList(bookId));

        assertEquals(HttpStatus.NO_CONTENT, commentDeletedResponse.getStatusCode());
    }

    @Test
    void should_return_not_found_when_delete_comment_not_exist() {
        ResponseEntity bookResponse = this.bookController.createBook(new Book("Test title"));
        Long bookId = getIdFromResponse(bookResponse);

        ResponseEntity commentDeletedResponse = this.commentController.deleteCommentOverBook(bookId, -1L);

        this.deleteBooks(Arrays.asList(bookId));

        assertEquals(HttpStatus.NOT_FOUND, commentDeletedResponse.getStatusCode());
    }

    @Test
    void should_return_not_found_when_delete_comment_not_exist_and_book() {
        ResponseEntity commentDeletedResponse = this.commentController.deleteCommentOverBook(-1L, -1L);

        assertEquals(HttpStatus.NOT_FOUND, commentDeletedResponse.getStatusCode());
    }


}
